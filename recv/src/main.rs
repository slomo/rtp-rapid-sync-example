// Copyright (c) 2022 Sebastian Dröge <sebastian@centricular.com>
//
// SPDX-License-Identifier: MIT

use gst::glib;
use gst::prelude::*;

use anyhow::{bail, Context, Error};

fn main() -> Result<(), Error> {
    // Initialize GStreamer.
    gst::init()?;

    // Get the RTSP URI from the first commandline argument.
    // Use e.g. "rtsp://192.168.1.101:8554/test".
    let uri = match std::env::args().nth(1) {
        Some(uri) if uri.starts_with("rtsp://") => uri,
        _ => bail!("No RTSP URI provided"),
    };

    // Create the NTP clock and wait for synchronization.
    let clock = gst_net::NtpClock::new(None, "pool.ntp.org", 123, gst::ClockTime::ZERO);
    println!("Syncing to NTP clock");
    clock
        .wait_for_sync(gst::ClockTime::from_seconds(5))
        .context("Syncing NTP clock")?;
    println!("Synced to NTP clock");

    // Create the playbin element...
    let pipeline = gst::ElementFactory::make("playbin", None)
        .context("Creating playbin")?
        .downcast::<gst::Pipeline>()
        .expect("playbin not a pipeline");

    // ... and set the RTSP URI on it.
    pipeline.set_property("uri", &uri);

    // Make sure it uses the NTP clock for synchronization.
    pipeline.use_clock(Some(&clock));

    // Configure a static latency of 2s. This needs to be the same on all receivers and higher than
    // the sum of the sender latency and the receiver latency of the receiver with the highest
    // latency. As this can't be known automatically and depends on many factors this has to be
    // known for the overall system and configured accordingly.
    pipeline.set_latency(gst::ClockTime::from_seconds(2));

    // When the RTSP source is created, configure a latency of 40ms instead
    // of the default of 2s on it and also add reference timestamp metadata
    // with the sender clock times to each packet if possible.
    pipeline.connect_closure(
        "source-setup",
        false,
        glib::closure!(|_playbin: &gst::Pipeline, source: &gst::Element| {
            source.set_property("latency", 40u32);
            source.set_property("add-reference-timestamp-meta", true);

            // Configure for network synchronization via the RTP NTP timestamps.
            // This requires that sender and receiver are synchronized to the same
            // clock.
            source.set_property_from_str("buffer-mode", "synced");
            source.set_property("ntp-sync", true);

            // Don't bother updating inter-stream offsets if the difference to the previous
            // configuration is less than 1ms. The synchronization will have rounding errors
            // in the range of the RTP clock rate, i.e. 1/90000s and 1/48000s in this case.
            source.connect_closure(
                "new-manager",
                false,
                glib::closure!(|_rtspsrc: &gst::Element, rtpbin: &gst::Element| {
                    rtpbin.set_property("min-ts-offset", gst::ClockTime::from_mseconds(1));
                }),
            );
        }),
    );

    // Create a timeoverlay element as custom video filter to render the
    // timestamps from the reference timestamp metadata on top of the video
    // frames in the bottom left.
    //
    // Also add a pad probe on its sink pad to print the same timestamp to
    // stdout on each frame.
    let timeoverlay =
        gst::ElementFactory::make("timeoverlay", None).context("Creating timeoverlay")?;
    timeoverlay.set_property_from_str("time-mode", "reference-timestamp");
    timeoverlay.set_property_from_str("valignment", "bottom");
    let sinkpad = timeoverlay
        .static_pad("video_sink")
        .expect("Failed to get timeoverlay sinkpad");
    sinkpad
        .add_probe(gst::PadProbeType::BUFFER, |_pad, info| {
            if let Some(gst::PadProbeData::Buffer(ref buffer)) = info.data {
                if let Some(meta) = buffer.meta::<gst::ReferenceTimestampMeta>() {
                    println!("Have sender clock time {}", meta.timestamp());
                } else {
                    println!("Have no sender clock time");
                }
            }

            gst::PadProbeReturn::Ok
        })
        .expect("Failed to add pad probe");

    pipeline.set_property("video-filter", &timeoverlay);

    // Start the pipeline.
    pipeline
        .set_state(gst::State::Playing)
        .context("Setting pipeline to playing")?;

    // And run until the stream is finished or there is an error.
    let bus = pipeline.bus().expect("Failed to get pipeline bus");
    let main_context = glib::MainContext::default();
    main_context.block_on(async {
        use futures::prelude::*;

        let mut bus_stream = bus.stream();

        // Handle messages from the pipeline to get notified about errors or
        // the stream having finished.
        while let Some(msg) = bus_stream.next().await {
            use gst::MessageView;

            match msg.view() {
                MessageView::Eos(_) => {
                    println!("Finished playback. Exiting.");
                    break;
                }
                MessageView::Error(msg) => {
                    let err = msg.error();
                    let src_name = msg.src().map(|src| src.name());

                    println!(
                        "ERROR from element {}: {err}",
                        src_name.as_deref().unwrap_or("UNKNOWN")
                    );
                    println!("Exiting.");
                    break;
                }
                _ => (),
            }
        }
    });

    // Shut down the pipeline.
    let _ = pipeline.set_state(gst::State::Null);

    Ok(())
}
