// Copyright (c) 2022 Sebastian Dröge <sebastian@centricular.com>
//
// SPDX-License-Identifier: MIT

use gst::glib;
use gst::gst_error;
use gst::prelude::*;
use gst_rtp::prelude::*;
use gst_rtsp_server::prelude::*;

use anyhow::{Context, Error};

use once_cell::sync::{Lazy, OnceCell};

static CAT: Lazy<gst::DebugCategory> = Lazy::new(|| {
    gst::DebugCategory::new(
        "rtp-rapid-sync-example",
        gst::DebugColorFlags::empty(),
        Some("RTP Rapid Sync Example"),
    )
});

// Singleton NTP clock used by the server
static CLOCK: OnceCell<gst_net::NtpClock> = OnceCell::new();

fn main() -> Result<(), Error> {
    // Initialize GStreamer.
    gst::init()?;

    // Create the NTP clock and wait for synchronization.
    let clock = gst_net::NtpClock::new(None, "pool.ntp.org", 123, gst::ClockTime::ZERO);
    println!("Syncing to NTP clock");
    clock
        .wait_for_sync(gst::ClockTime::from_seconds(5))
        .context("Syncing NTP clock")?;
    println!("Synced to NTP clock");
    CLOCK.set(clock).expect("Failed to store clock");

    // Create an RTSP server with the default settings.
    let server = gst_rtsp_server::RTSPServer::new();

    // Create a new instance of our media factory and place it on the `/test` path
    // for this server.
    let mounts = server.mount_points().expect("server has no mount points");
    let factory = media_factory::Factory::default();
    factory.set_shared(true);
    mounts.add_factory("/test", &factory);

    // Attach server to the default main context.
    let main_context = glib::MainContext::default();
    let _id = server.attach(Some(&main_context))?;

    // And run forever.
    main_context.block_on(async {
        futures::future::pending::<()>().await;
    });

    Ok(())
}

// Custom media factory that creates medias of a custom type and that creates a custom
// stream producer pipeline with a H264 video stream and an Opus audio stream.
mod media_factory {
    use super::*;

    use gst_rtsp_server::subclass::prelude::*;

    mod imp {
        use super::*;

        #[derive(Default)]
        pub struct Factory;

        #[glib::object_subclass]
        impl ObjectSubclass for Factory {
            const NAME: &'static str = "MyRTSPMediaFactory";
            type Type = super::Factory;
            type ParentType = gst_rtsp_server::RTSPMediaFactory;
        }

        impl ObjectImpl for Factory {
            fn constructed(&self, factory: &Self::Type) {
                self.parent_constructed(factory);

                // Let the factory create medias of our custom media type.
                factory.set_media_gtype(super::media::Media::static_type());
                // Make sure the media factory uses the NTP clock.
                factory.set_clock(Some(CLOCK.get().expect("No clock set")));
            }
        }

        impl RTSPMediaFactoryImpl for Factory {
            // Create the pipeline and configure it.
            fn create_pipeline(
                &self,
                factory: &Self::Type,
                media: &gst_rtsp_server::RTSPMedia,
            ) -> Option<gst::Pipeline> {
                let pipeline = self.parent_create_pipeline(factory, media)?;

                // Set the base time of the pipeline statically to zero so that running
                // time and clock time are the same and timeoverlay can be used to render
                // the clock time over the video frames.
                //
                // This is needed for no other reasons.
                pipeline.set_base_time(gst::ClockTime::ZERO);
                pipeline.set_start_time(gst::ClockTime::NONE);

                Some(pipeline)
            }

            // Create the custom stream producer bin.
            fn create_element(
                &self,
                factory: &Self::Type,
                _url: &gst_rtsp::RTSPUrl,
            ) -> Option<gst::Element> {
                match self.create_element() {
                    Err(err) => {
                        gst_error!(CAT, obj: factory, "Error creating RTSP elements: {err}");
                        None
                    }
                    Ok(element) => Some(element),
                }
            }
        }

        impl Factory {
            fn create_element(&self) -> Result<gst::Element, Error> {
                // Stream producer bin.
                let bin = gst::Bin::new(None);

                // Video part of the pipeline:
                //   videotestsrc ! timeoverlay ! x264enc ! rtph264pay
                let video_src = gst::ElementFactory::make("videotestsrc", None)
                    .context("Creating videotestsrc")?;
                let video_overlay = gst::ElementFactory::make("timeoverlay", None)
                    .context("Creating timeoverlay")?;
                let video_enc =
                    gst::ElementFactory::make("x264enc", None).context("Creating x264enc")?;
                let video_pay = gst::ElementFactory::make("rtph264pay", Some("pay0"))
                    .context("Creating rtph264pay")?;

                // Pretend to be a live stream.
                video_src.set_property("is-live", true);
                // Render the running-time over the video frames on the top left. By setting the
                // base time to zero further above the running time will be equal to the clock
                // time.
                video_overlay.set_property_from_str("time-mode", "running-time");
                // Configure encoder to not introduce any latency and to create a keyframe
                // every 30 frames.
                video_enc.set_property_from_str("tune", "zerolatency");
                video_enc.set_property("key-int-max", 30u32);
                // Configure this RTP stream to payload type 96.
                video_pay.set_property("pt", 96u32);

                // Add RFC6051 64-bit NTP timestamp RTP header extension.
                let hdr_ext = gst_rtp::RTPHeaderExtension::create_from_uri(
                    "urn:ietf:params:rtp-hdrext:ntp-64",
                )
                .context("Creating NTP 64-bit RTP header extension")?;
                hdr_ext.set_id(1);
                video_pay.emit_by_name::<()>("add-extension", &[&hdr_ext]);

                bin.add_many(&[&video_src, &video_overlay, &video_enc, &video_pay])
                    .expect("Failed to add video elements to bin");

                // Link the video source with the next element via a capsfilter to make sure it
                // produces 800x600 video frames.
                video_src
                    .link_filtered(
                        &video_overlay,
                        &gst::Caps::builder("video/x-raw")
                            .field("width", 800i32)
                            .field("height", 600i32)
                            .build(),
                    )
                    .context("Linking videotestsrc to timeoverlay")?;
                video_overlay
                    .link(&video_enc)
                    .context("Linking timeoverlay to x264enc")?;
                video_enc
                    .link(&video_pay)
                    .context("Linking x264enc to rtph264pay")?;

                // Audio part of the pipeline:
                //   audiotestsrc ! opusenc ! rtopuspay
                let audio_src = gst::ElementFactory::make("audiotestsrc", None)
                    .context("Creating audiotestsrc")?;
                let audio_enc =
                    gst::ElementFactory::make("opusenc", None).context("Creating opusenc")?;
                let audio_pay = gst::ElementFactory::make("rtpopuspay", Some("pay1"))
                    .context("Creating rtpopuspay")?;

                // Pretend to be a live stream.
                audio_src.set_property("is-live", true);
                // Configure this RTP stream to payload type 97.
                audio_pay.set_property("pt", 97u32);

                // Add RFC6051 64-bit NTP timestamp RTP header extension.
                let hdr_ext = gst_rtp::RTPHeaderExtension::create_from_uri(
                    "urn:ietf:params:rtp-hdrext:ntp-64",
                )
                .context("Creating NTP 64-bit RTP header extension")?;
                hdr_ext.set_id(1);
                audio_pay.emit_by_name::<()>("add-extension", &[&hdr_ext]);

                bin.add_many(&[&audio_src, &audio_enc, &audio_pay])
                    .expect("Failed to add audio elements to bin");

                audio_src
                    .link(&audio_enc)
                    .context("Linking audiotestsrc to opusenc")?;
                audio_enc
                    .link(&audio_pay)
                    .context("Linking opusenc to rtpopuspay")?;

                Ok(bin.upcast())
            }
        }
    }

    glib::wrapper! {
        pub struct Factory(ObjectSubclass<imp::Factory>) @extends gst_rtsp_server::RTSPMediaFactory;
    }

    // Trivial constructor for the media factory.
    impl Default for Factory {
        fn default() -> Factory {
            glib::Object::new(&[]).expect("Failed to create factory")
        }
    }
}

// Custom media. This doesn't do anything special yet and is instantiated by the media factory
// above.
mod media {
    use super::*;
    use gst_rtsp_server::subclass::prelude::*;

    mod imp {
        use super::*;

        #[derive(Default)]
        pub struct Media {}

        #[glib::object_subclass]
        impl ObjectSubclass for Media {
            const NAME: &'static str = "MyRTSPMedia";
            type Type = super::Media;
            type ParentType = gst_rtsp_server::RTSPMedia;
        }

        impl ObjectImpl for Media {}

        impl RTSPMediaImpl for Media {
            fn setup_rtpbin(
                &self,
                media: &Self::Type,
                rtpbin: &gst::Element,
            ) -> Result<(), gst::LoggableError> {
                self.parent_setup_rtpbin(media, rtpbin)?;

                // Use local pipeline clock time as RTP NTP time source instead of using
                // the local wallclock time converted to the NTP epoch.
                rtpbin.set_property_from_str("ntp-time-source", "clock-time");

                // Use the capture time instead of the send time for the RTP / NTP timestamp
                // mapping. The difference between the two options is the capture/encoder/etc.
                // latency that is introduced before sending.
                rtpbin.set_property("rtcp-sync-send-time", false);

                Ok(())
            }
        }
    }

    glib::wrapper! {
        pub struct Media(ObjectSubclass<imp::Media>) @extends gst_rtsp_server::RTSPMedia;
    }
}
