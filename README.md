# GStreamer RTP Rapid Synchronization Example

This repository contains the example code for my blog post about [RTP Rapid Synchronization](https://coaxion.net/blog/2022/05/instantaneous-rtp-synchronization-retrieval-of-absolute-sender-clock-times-with-gstreamer).

Please read the blog post for details.

The code below needs GStreamer from the git `main` branch at the time of
writing, or version 1.22 or newer.

## Inter-device network synchronization via NTP

This branch contains the code that retrieves the sender clock times on the
receiver and renders them over the video frames on the receiver and also
prints them on stdout. The times are synchronized between the sender and
receiver via an RTP header extension from the very beginning instead of
relying on RTCP.

In addition the sender and receiver are synchronized to the same NTP clock and
the receiver is configured to do inter-device synchronization based on this.
If multiple receivers are running next to each other they will all play back
the stream completely synchronized, and in this configuration with exactly 2s
latency compared to when the frames were captured.

Both timestamps on the video frames should be the same: the one rendered on
the sender at the top and the one rendered on the receiver at the bottom.

![Screenshot](screenshot.png)

Run the sender with

```console
$ cargo run -p rtp-rapid-sync-example-send
```

This will start an RTSP server on the local machine that listens on port
`8554`.

Afterwards the receiver can be run on the same or a different machine with

```console
$ cargo run -p rtp-rapid-sync-example-send -- rtsp://192.168.1.101:8554/test
```

Please replace the IP with the IP of the machine that is running the server.

## LICENSE

All code in this repository is licensed under the MIT license. See ([LICENSE](LICENSE) or http://opensource.org/licenses/MIT).

